import EventEmitter from './EventEmitter.js'

export default class Time extends EventEmitter {
    #INIT_DELTA = 16

    constructor() {
        super()

        // Setup
        this.start = Date.now()
        this.current = this.start
        this.elapsedTime = 0
        this.delta = this.#INIT_DELTA

        window.requestAnimationFrame(() => this.tick())
    }

    tick() {
        const currentTime = Date.now()
        this.delta = currentTime - this.current
        this.current = currentTime
        this.elapsedTime = this.current - this.start

        this.trigger('tick')

        window.requestAnimationFrame(() => this.tick())
    }
}
