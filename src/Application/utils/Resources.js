import * as THREE from 'three'
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js'
import { RGBELoader } from 'three/addons/loaders/RGBELoader.js'

import gsap from 'gsap'

import EventEmitter from './EventEmitter.js'
import Application from '../Application.js'

export default class Resources extends EventEmitter {
    constructor(assets) {
        super()

        this.application = new Application()
        this.scene = this.application.scene

        // Options
        this.assets = assets

        // Setup
        this.items = {}
        this.toLoad = this.assets.length
        this.loaded = 0

        this.setOverley()
        this.setLoaders()
        this.startLoading()
    }

    setOverley() {
        const overlayGeometry = new THREE.PlaneGeometry(2, 2, 1, 1)
        this.overlayMaterial = new THREE.ShaderMaterial({
            transparent: true,
            uniforms: {
                uAlpha: {
                    value: 1,
                },
            },
            vertexShader: `
                void main()
                {
                    gl_Position = vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                uniform float uAlpha;
                
                void main()
                {
                    gl_FragColor = vec4(255.0, 255.0, 255.0, uAlpha);
                }
            `,
        })
        this.overlay = new THREE.Mesh(overlayGeometry, this.overlayMaterial)
        this.scene.add(this.overlay)
    }

    setLoaders() {
        this.loaders = {}

        const progressBar = document.querySelector('.loading-bar')
        const loadingManager = new THREE.LoadingManager(
            () => {
                gsap.delayedCall(0.5, () => {
                    gsap.to(this.overlayMaterial.uniforms.uAlpha, { duration: 3, value: 0 })
                    progressBar.classList.add('ended')
                    progressBar.style.transform = ''
                })
            },
            (itemUrl, itemLoaded, itemTotal) => {
                const progressRatio = itemLoaded / itemTotal
                progressBar.style.transform = `scaleX(${progressRatio})`
            }
        )

        this.loaders.glftLoader = new GLTFLoader(loadingManager)
        this.loaders.rgbeLoader = new RGBELoader(loadingManager)
        this.loaders.textureLoader = new THREE.TextureLoader(loadingManager)
    }

    startLoading() {
        for (const asset of this.assets) {
            switch (asset.type) {
                case 'glbModel':
                case 'glftModel':
                case 'fbxModel':
                    this.loaders.glftLoader.load(asset.path, (file) => this.assetLoaded(asset, file))
                    break
                case 'hdrTexture':
                    this.loaders.rgbeLoader.load(asset.path, (file) => this.assetLoaded(asset, file))
                    break
                case 'texture':
                    this.loaders.textureLoader.load(asset.path, (file) => this.assetLoaded(asset, file))
                    break
            }
        }
    }

    assetLoaded(asset, file) {
        this.items[asset.name] = file

        this.loaded++

        if (this.loaded === this.toLoad) {
            this.trigger('loaded')
        }
    }
}
