import Application from '../Application'

export default class UI {
    constructor() {
        // Setup
        this.application = new Application()
        this.sofa = this.application.world.sofa
        this.floor = this.application.world.floor

        // DOM
        this.textures = document.querySelectorAll('.textures .demo')
        this.colors = document.querySelectorAll('.colors .demo')

        // Listeners
        this.textures.forEach((texture) => {
            texture.addEventListener('click', (e) => this.updateTextures(e))
        })
        this.colors.forEach((color) => {
            color.addEventListener('click', (e) => this.updateColor(e))
        })
    }

    updateTextures(e) {
        const textureName = e.currentTarget.getAttribute('data-texture')
        if (textureName === 'no-texture') {
            this.sofa.clearTextures()
        } else {
            this.sofa.updateTextures(textureName)
        }
    }

    updateColor(e) {
        const colorName = e.currentTarget.getAttribute('data-color')
        this.sofa.updateColor(colorName)
    }
}
