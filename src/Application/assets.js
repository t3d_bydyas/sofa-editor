export default [
    // Sofa Model
    {
        name: 'sofaModel',
        type: 'glbModel',
        path: '/models/sofa/sofa-model.glb',
    },
    {
        name: 'environmentMapTexture',
        type: 'hdrTexture',
        path: '/textures/environmentMap/studio_small_08_1k.hdr',
    },
    // Fabric Pattern Texture
    {
        name: 'fabricColorTexture',
        type: 'texture',
        path: '/textures/fabric_pattern/fabric_pattern_07_col_1_1k.jpg',
    },
    {
        name: 'fabricAoTexture',
        type: 'texture',
        path: '/textures/fabric_pattern/fabric_pattern_07_ao_1k.jpg',
    },
    {
        name: 'fabricNormTexture',
        type: 'texture',
        path: '/textures/fabric_pattern/fabric_pattern_07_nor_gl_1k.png',
    },
    {
        name: 'fabricRoughTexture',
        type: 'texture',
        path: '/textures/fabric_pattern/fabric_pattern_07_rough_1k.jpg',
    },
    // Book Pattern Texture
    {
        name: 'bookColorTexture',
        type: 'texture',
        path: '/textures/book_pattern/book_pattern_col1_1k.jpg',
    },
    {
        name: 'bookAoTexture',
        type: 'texture',
        path: '/textures/book_pattern/book_pattern_ao_1k.jpg',
    },
    {
        name: 'bookNormTexture',
        type: 'texture',
        path: '/textures/book_pattern/book_pattern_nor_gl_1k.png',
    },
    {
        name: 'bookRoughTexture',
        type: 'texture',
        path: '/textures/book_pattern/book_pattern_rough_1k.jpg',
    },
    // Leather Red Texture
    {
        name: 'leatherColorTexture',
        type: 'texture',
        path: '/textures/leather_red/leather_red_03_coll1_1k.jpg',
    },
    {
        name: 'leatherAoTexture',
        type: 'texture',
        path: '/textures/leather_red/leather_red_03_ao_1k.jpg',
    },
    {
        name: 'leatherNormTexture',
        type: 'texture',
        path: '/textures/leather_red/leather_red_03_nor_gl_1k.png',
    },
    {
        name: 'leatherRoughTexture',
        type: 'texture',
        path: '/textures/leather_red/leather_red_03_rough_1k.jpg',
    },
    // Floor Texture
    {
        name: 'floorColorTexture',
        type: 'texture',
        path: '/textures/floor/wood_cabinet_worn_long_diff_1k.jpg',
    },
    {
        name: 'floorAoTexture',
        type: 'texture',
        path: '/textures/floor/wood_cabinet_worn_long_ao_1k.jpg',
    },
    {
        name: 'floorNormTexture',
        type: 'texture',
        path: '/textures/floor/wood_cabinet_worn_long_nor_gl_1k.png',
    },
    {
        name: 'floorRoughTexture',
        type: 'texture',
        path: '/textures/floor/wood_cabinet_worn_long_rough_1k.jpg',
    },
]
