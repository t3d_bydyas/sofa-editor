import * as THREE from 'three'

import Application from '../Application'

export default class Floor {
    constructor() {
        // Setup
        this.application = new Application()
        this.scene = this.application.scene
        this.resourceItems = this.application.resources.items

        this.setGeometry()
        this.setTextures()
        this.setMaterial()
        this.setMesh()
    }

    setGeometry() {
        this.geometry = new THREE.CircleGeometry(2.5, 128)
    }

    setTextures() {
        this.textures = {}
        const repeatIndex = 4

        this.textures.colorTexture = this.resourceItems.floorColorTexture
        this.textures.colorTexture.colorSpace = THREE.SRGBColorSpace
        this.textures.colorTexture.repeat.set(repeatIndex, repeatIndex)
        this.textures.colorTexture.wrapS = THREE.RepeatWrapping
        this.textures.colorTexture.wrapT = THREE.RepeatWrapping

        this.textures.aoTexture = this.resourceItems.floorAoTexture
        this.textures.aoTexture.colorSpace = THREE.SRGBColorSpace
        this.textures.aoTexture.repeat.set(repeatIndex, repeatIndex)
        this.textures.aoTexture.wrapS = THREE.RepeatWrapping
        this.textures.aoTexture.wrapT = THREE.RepeatWrapping

        this.textures.normalTexture = this.resourceItems.floorNormTexture
        this.textures.normalTexture.colorSpace = THREE.SRGBColorSpace
        this.textures.normalTexture.repeat.set(repeatIndex, repeatIndex)
        this.textures.normalTexture.wrapS = THREE.RepeatWrapping
        this.textures.normalTexture.wrapT = THREE.RepeatWrapping

        this.textures.roughTexture = this.resourceItems.floorRoughTexture
        this.textures.roughTexture.colorSpace = THREE.SRGBColorSpace
        this.textures.roughTexture.repeat.set(repeatIndex, repeatIndex)
        this.textures.roughTexture.wrapS = THREE.RepeatWrapping
        this.textures.roughTexture.wrapT = THREE.RepeatWrapping
    }

    setMaterial() {
        this.material = new THREE.MeshStandardMaterial({
            map: this.textures.colorTexture,
            aoMap: this.textures.aoTexture,
            normalMap: this.textures.normalMap,
            roughnessMap: this.textures.roughTexture,
        })
    }

    setMesh() {
        this.mesh = new THREE.Mesh(this.geometry, this.material)
        this.mesh.receiveShadow = true
        this.mesh.rotation.x = -Math.PI / 2
        this.scene.add(this.mesh)
    }
}
