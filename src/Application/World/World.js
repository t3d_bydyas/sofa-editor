import Application from '../Application'
import UI from '../utils/UI'
import Environment from './Environment'
import Floor from './Floor'
import Sofa from './Sofa'

export default class World {
    constructor() {
        this.application = new Application()

        // Setup
        this.resources = this.application.resources
        this.scene = this.application.scene

        // Listener
        this.resources.on('loaded', () => {
            // World's objects
            this.sofa = new Sofa()
            this.floor = new Floor()
            this.environment = new Environment()

            this.ui = new UI()
        })
    }
}
