import * as THREE from 'three'

import Application from '../Application'

export default class Environment {
    constructor() {
        this.application = new Application()
        this.scene = this.application.scene
        this.resources = this.application.resources

        this.setLight()
        this.setEnvironmentMap()
    }

    setEnvironmentMap() {
        this.environmentMap = this.resources.items.environmentMapTexture
        this.environmentMap.mapping = THREE.EquirectangularReflectionMapping

        this.scene.environment = this.environmentMap
    }

    setLight() {
        const light = new THREE.DirectionalLight('#f7f7f7', 3)
        light.castShadow = true
        light.shadow.mapSize.set(1024, 1024)
        light.shadow.camera.far = 3
        light.shadow.camera.left = -3
        light.shadow.camera.top = 3
        light.shadow.camera.right = 3
        light.shadow.camera.bottom = -3
        this.scene.add(light)
    }
}
