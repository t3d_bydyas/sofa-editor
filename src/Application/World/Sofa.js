import * as THREE from 'three'

import Application from '../Application'

export default class Sofa {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.resources = this.application.resources

        this.setTextures()
        this.setModel()
    }

    setTextures(name = 'leather') {
        this.textures = {}

        // Sofa pillow textures
        this.textures.pillowColorTexture = this.resources.items[`${name}ColorTexture`]
        this.textures.pillowColorTexture.wrapS = THREE.MirroredRepeatWrapping
        this.textures.pillowColorTexture.wrapT = THREE.MirroredRepeatWrapping

        this.textures.pillowAoTexture = this.resources.items[`${name}AoTexture`]
        this.textures.pillowAoTexture.wrapS = THREE.MirroredRepeatWrapping
        this.textures.pillowAoTexture.wrapT = THREE.MirroredRepeatWrapping

        this.textures.pillowNormTexture = this.resources.items[`${name}NormTexture`]
        this.textures.pillowNormTexture.wrapS = THREE.MirroredRepeatWrapping
        this.textures.pillowNormTexture.wrapT = THREE.MirroredRepeatWrapping

        this.textures.pillowRoughTexture = this.resources.items[`${name}RoughTexture`]
        this.textures.pillowRoughTexture.wrapS = THREE.MirroredRepeatWrapping
        this.textures.pillowRoughTexture.wrapT = THREE.MirroredRepeatWrapping
    }

    clearTextures() {
        this.model.traverse((child) => {
            if (child.material) {
                child.material.map = undefined
                child.material.aoMap = undefined
                child.material.normalMap = undefined
                child.material.roughnessMap = undefined
                child.material.needsUpdate = true
            }
        })
    }

    updateTextures(name) {
        this.setTextures(name)

        this.model.traverse((child) => {
            if (child.material) {
                child.material.map = this.textures.pillowColorTexture
                child.material.aoMap = this.textures.pillowAoTexture
                child.material.normalMap = this.textures.pillowNormTexture
                child.material.roughnessMap = this.textures.pillowRoughTexture
                child.material.needsUpdate = true
            }
        })
    }

    updateColor(hex) {
        this.color = new THREE.Color(hex)

        this.model.traverse((child) => {
            if (child.material) {
                child.material.color = this.color
                child.material.needsUpdate = true
            }
        })
    }

    setModel() {
        this.model = this.resources.items.sofaModel.scene
        this.model.position.y = 0.01

        this.model.traverse((child) => {
            if (child.material) {
                child.material.map = this.textures.pillowColorTexture
                child.material.aoMap = this.textures.pillowAoTexture
                child.material.normalMap = this.textures.pillowNormTexture
                child.material.roughnessMap = this.textures.pillowRoughTexture
            }
            if (child.isMesh) {
                child.castShadow = true
            }
        })

        this.scene.add(this.model)
    }
}
