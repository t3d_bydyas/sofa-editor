import * as THREE from 'three'
import { OrbitControls } from 'three/addons/controls/OrbitControls.js'

import Application from './Application.js'

export default class Camera {
    fov = 75
    near = 0.01
    far = 100

    constructor() {
        this.application = new Application()

        // Setup
        this.sizes = this.application.sizes
        this.scene = this.application.scene
        this.canvas = this.application.canvas

        this.setInstance()
        this.setOrbitControls()
    }

    setInstance() {
        this.instance = new THREE.PerspectiveCamera(this.fov, this.sizes.width / this.sizes.height, this.near, this.far)
        this.instance.position.set(-1, 1, 2)
        this.scene.add(this.instance)
    }

    setOrbitControls() {
        this.controls = new OrbitControls(this.instance, this.canvas)
        this.controls.enablePan = false
        this.controls.maxPolarAngle = Math.PI / 2.1
        this.controls.minDistance = 2.7
        this.controls.maxDistance = 5
        this.controls.enableDamping = true
    }

    resize() {
        this.instance.aspect = this.sizes.width / this.sizes.height
        this.instance.updateProjectionMatrix()
    }

    update() {
        this.controls.update()
    }
}
