import * as THREE from 'three'

import Sizes from './utils/Sizes.js'
import Time from './utils/Time.js'
import Resources from './utils/Resources.js'
import Camera from './Camera.js'
import Renderer from './Renderer.js'

import assets from './assets.js'
import World from './World/World.js'

THREE.ColorManagement.enabled = false

export default class Application {
    static instance

    constructor(canvas) {
        // Singleton
        if (Application.instance) {
            return Application.instance
        }
        Application.instance = this

        // Global access
        window.application = this

        // Options
        this.canvas = canvas

        // Setup
        this.sizes = new Sizes()
        this.time = new Time()
        this.scene = new THREE.Scene()
        this.resources = new Resources(assets)
        this.camera = new Camera()
        this.renderer = new Renderer()
        this.world = new World()

        // Listeners
        this.sizes.on('resize', () => this.resize())
        this.time.on('tick', () => this.update())
    }

    resize() {
        this.camera.resize()
        this.renderer.resize()
    }

    update() {
        this.camera.update()
        this.renderer.update()
    }
}
