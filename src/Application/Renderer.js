import * as THREE from 'three'

import Application from './Application'

export default class Renderer {
    constructor() {
        this.application = new Application()

        // Setup
        this.canvas = this.application.canvas
        this.sizes = this.application.sizes
        this.camera = this.application.camera
        this.scene = this.application.scene

        this.setInstance()
    }

    setInstance() {
        this.instance = new THREE.WebGLRenderer({
            canvas: this.canvas,
            antialias: true,
        })
        this.instance.outputColorSpace = THREE.LinearSRGBColorSpace
        this.instance.toneMapping = THREE.ACESFilmicToneMapping
        this.instance.toneMappingExposure = 1.2
        this.instance.setClearColor('#f7f7f7')
        this.instance.shadowMap.enabled = true
        this.instance.shadowMap.type = THREE.PCFSoftShadowMap
        this.instance.setSize(this.sizes.width, this.sizes.height)
        this.instance.setPixelRatio(Math.min(window.devicePixelRatio, this.sizes.PIXEL_RATIO_LIMIT))
    }

    resize() {
        this.instance.setSize(this.sizes.width, this.sizes.height)
        this.instance.setPixelRatio(Math.min(window.devicePixelRatio, this.sizes.PIXEL_RATIO_LIMIT))
    }

    update() {
        this.instance.render(this.scene, this.camera.instance)
    }
}
